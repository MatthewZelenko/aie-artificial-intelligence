#include "Farm.h"

Farm::Farm(float a_maxFood) : Site("Farm")
{
	m_Food = 0;
	m_MaxFood = a_maxFood;
	m_Colour = glm::vec4(0.9f, 0.9f, 0.2f, 1.0f);
	m_Size = glm::vec3(20, 2, 20);
	m_IsGrowing = true;
}
Farm::~Farm()
{

}

void Farm::Update(float a_deltaTime)
{

}
void Farm::Draw(Debugger& a_debugger)
{
	m_Colour.a = ((m_Food / m_MaxFood) * 0.75f) + 0.25f;
	if(m_Food > 0)
 		a_debugger.AddCube(m_Position, m_Size, m_Colour);
}

void Farm::AddFood(float a_food)
{
	m_Food = glm::clamp(m_Food + a_food, 0.0f, m_MaxFood);
}
float Farm::TakeFood(float a_food)
{
	if (m_Food == 0 || a_food == 0 || m_IsGrowing)
		return 0;

	m_Food -= a_food;
	float foodReturn = a_food;
	if (m_Food < 0)
	{
		foodReturn = a_food - m_Food;
		m_Food = 0;
	}
	return foodReturn;
}

void Farm::SetIsGrowing(bool a_value)
{
	m_IsGrowing = a_value;
}