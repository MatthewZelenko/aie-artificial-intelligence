#include "WellCollector.h"
#include "World.h"
#include "Well.h"

WellCollector::WellCollector() : Pawn("WellCollector")
{
	m_CollectedWater = 0.0f;
	m_Colour = glm::vec4(0.0f, 0.6f, 0.5f, 1.0f);
	m_Well = nullptr;
}
WellCollector::~WellCollector()
{

}

void WellCollector::Init(World* a_world, float a_maxCollectedWater, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed)
{
	Pawn::Init(a_world, a_maxFood, a_maxWater, a_maxRest, a_speed);
	m_MaxCollectedWater = a_maxCollectedWater;
}
void WellCollector::Update(float a_deltaTime)
{
	DecreaseStats(a_deltaTime);
	SelectAction();
	DoAction(a_deltaTime);
}

void WellCollector::SelectAction()
{
	if (!m_IsBusy)
	{
		Warehouse* warehouse = m_World->GetWarehouse();
		float highestValue = 0.0f;
		m_Action = "";

		float wellValue = 70.0f;
		if (wellValue > highestValue)
		{
			m_Action = "CollectWater";
			highestValue = wellValue;
		}

		float eatValue = EvaluateHunger();
		if (eatValue > highestValue)
		{
			m_Action = "Eat";
			highestValue = eatValue;
		}

		float drinkValue = EvaluateThirst();
		if (drinkValue > highestValue && warehouse->GetWater() > 0)
		{
			m_Action = "Drink";
			highestValue = drinkValue;
		}

		float restValue = EvaluateSleep();
		if (restValue > highestValue)
		{
			m_Action = "Rest";
			highestValue = restValue;
		}
	}
}
void WellCollector::DoAction(float a_deltaTime)
{
	Pawn::DoAction(a_deltaTime);
	if (m_Action == "CollectWater")
	{
		//If empty handed
		if (m_CollectedWater == 0)
		{
			//Get well
			if (!m_Well || (m_Well->GetWater() <= 0.0f && !m_Well->IsBuilding()))
				m_Well = m_World->GetNearestWell(m_Position);
			if (m_Well)
			{
				if (m_World->GetWarehouse()->GetWater() == 0)
				{
					m_IsBusy = true;
				}
				glm::vec3 direction = m_Well->GetPosition() - m_Position;
				float distance = glm::length(direction);
				direction = glm::normalize(direction);
				//If distance to well is greater than 0 move towards well
				if (distance > 0)
				{
					float speed = a_deltaTime * m_Speed;
					//if speed is greater than distance then just jump to well
					if (speed >= distance)
					{
						m_Position = m_Well->GetPosition();
					}
					//else move towards it normally
					else
					{
						m_Position += direction * speed;
					}
				}
				//else if pawn is at well
				else
				{
					if (m_Well->IsBuilding())
					{
						m_Well->AddWater(a_deltaTime * 500);
						if (m_Well->GetWater() >= m_Well->GetMaxWater())
							m_Well->SetIsBuilding(false);
					}
					else
					{
						//how much water can we take?
						float water = m_Well->TakeWater(m_MaxCollectedWater - glm::max(m_CollectedWater, 0.0f));
						//Bon Apetite'
						m_CollectedWater += water;
					}
				}
			}
		}
		//else
		else
		{
			m_IsBusy = true;
			//Get warehouse
			Warehouse* warehouse = m_World->GetWarehouse();
			glm::vec3 direction = warehouse->GetPosition() - m_Position;
			float distance = glm::length(direction);
			direction = glm::normalize(direction);
			//If distance to warehouse is greater than 0 move towards warehouse
			if (distance > 0)
			{
				float speed = a_deltaTime * m_Speed;
				//if speed is greater than distance then just jump to warehouse
				if (speed >= distance)
				{
					m_Position = warehouse->GetPosition();
				}
				//else move towards it normally
				else
				{
					m_Position += direction * speed;
				}
			}
			//else if pawn is at warehouse
			else
			{
				//give food
				warehouse->AddWater(m_CollectedWater);
				m_CollectedWater = 0.0f;
				m_IsBusy = false;
			}
		}
	}
}

void WellCollector::SetMaxCollectedWater(float a_max)
{
	m_MaxCollectedWater = a_max;
}
void WellCollector::SetCollectedWater(float a_value)
{
	m_CollectedWater = glm::clamp(a_value, 0.0f, m_MaxCollectedWater);
}

void WellCollector::AddCollectedWater(float a_value)
{
	m_CollectedWater = glm::clamp(m_CollectedWater + a_value, 0.0f, m_MaxCollectedWater);
}