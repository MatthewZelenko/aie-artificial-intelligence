#include "AI Assessment.h"
#include <GLM\gtc\matrix_transform.hpp>
#include "Input.h"
#include <time.h>

AIAssessment::AIAssessment(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight) : Application(a_gameTitle, a_gameWidth, a_gameHeight)
{

}
AIAssessment::~AIAssessment()
{
	
}

void AIAssessment::Load()
{
	srand((unsigned int)time(NULL));
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Camera
	m_Camera = FPSCamera(m_GameWindow, 200.0f, 200.0f, 0.002f, 50.0f);
	m_Camera.SetInputKeys(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_KEY_SPACE, GLFW_MOUSE_BUTTON_2);
	m_Camera.SetPerspective(45.0f, (float)m_GameWidth / (float)m_GameHeight, 0.1f, 10000.0f);
	m_Camera.SetView(glm::vec3(0.0f, 100.0f, 100.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	m_Camera.SetJumpPower(200);

	//Debugger
	m_Debugger.Init(m_Camera.GetProjection());
	m_Debugger.CreateGrid(10, 10, glm::vec4(0.2f, 0.5f, 1.0f, 1.0f));

	//Terrain Shader
	m_TerrainShader.CreateFromFile("resources/shaders/", "Terrain.vert", nullptr, "Terrain.frag");
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("projection", 1, m_Camera.GetProjection());
	m_TerrainShader.SendMat4("view", 1, m_Camera.GetView());

	//Terrain
	m_Terrain.SetLandAmplitude(100.0f);
	m_Terrain.SetLandFrequency(0.005f);
	m_Terrain.SetLandPersistence(0.25f);
	m_Terrain.SetLandOctave(8);
	m_Terrain.SetLandSeed(150);
	m_Terrain.Init(200, 200, 80.0f, 80.0f);

	//World
	m_World.Init(m_Camera, m_Terrain);
	
}
void AIAssessment::ProcessInput()
{

}
void AIAssessment::Update()
{
	m_Camera.Update((float)GetDeltaTime());

	//Camera height
	float yPos = m_Terrain.GetLandHeightFromPosition(m_Camera.GetPosition().x, m_Camera.GetPosition().z, true);
	if (m_Camera.GetPosition().y <= yPos + 20)
	{
		m_Camera.SetIsGrounded(true);
		m_Camera.SetYPosition(yPos + 20);
	}
	else
	{
		m_Camera.SetIsGrounded(false);
	}

	m_World.Update((float)GetDeltaTime(), m_Terrain);
}
void AIAssessment::Render()
{
	//Light
	glm::vec3 lightPos = glm::vec3(cos(glfwGetTime() / 10.0f) * 100000, sin(glfwGetTime() / 10.0f) * 100000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.75f, 0.75f);

	//Terrain
	m_TerrainShader.Use();
	m_TerrainShader.SendMat4("view", 1, m_Camera.GetView());
	m_TerrainShader.SendVec3("cameraPos", 1, m_Camera.GetPosition());
	m_TerrainShader.SendVec3("lightPos", 1, lightPos);
	m_TerrainShader.SendVec3("lightColour", 1, lightColour);
	m_TerrainShader.SendFloat("specularStr", 0.25f);
	m_TerrainShader.SendFloat("ambientStr", 0.1f);
	m_Terrain.DrawLand(m_TerrainShader);

	//Borders - useless, but looks kool
	//top
	m_Debugger.AddCube(glm::vec3(0.0f, 100.0f, -600.0f), glm::vec3(-1200.0f, 400.0f, -8.0f), glm::vec4(1.0f, 0.5f, 0.0f, 0.1f));
	//right
	m_Debugger.AddCube(glm::vec3(600.0f, 100.0f, 0.0f), glm::vec3(8.0f, 400.0f, 1200.0f), glm::vec4(1.0f, 0.5f, 0.0f, 0.1f));
	//bottom
	m_Debugger.AddCube(glm::vec3(0.0f, 100.0f, 600.0f), glm::vec3(1200.0f, 400.0f, 8.0f), glm::vec4(1.0f, 0.5f, 0.0f, 0.1f));
	//left
	m_Debugger.AddCube(glm::vec3(-600.0f, 100.0f, 0.0f), glm::vec3(-8.0f, 400.0f, -1200.0f), glm::vec4(1.0f, 0.5f, 0.0f, 0.1f));

	m_World.Draw(m_Debugger, m_Camera);

	//Debugger
	m_Debugger.Draw(m_Camera.GetView());
	m_Debugger.ClearObjects();
}
void AIAssessment::UnLoad()
{
	m_Terrain.Destroy();
}