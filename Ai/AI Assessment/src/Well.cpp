#include "Well.h"

Well::Well(float a_maxWater) : Site("Well")
{
	m_Water = 0;
	m_MaxWater = a_maxWater;
	m_Colour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
	m_Size = glm::vec3(10, 15, 10);
	m_IsBuilding = true;
}
Well::~Well()
{
}

void Well::Update(float a_deltaTime)
{

}
void Well::Draw(Debugger& a_debugger)
{
	m_Colour.a = ((m_Water / m_MaxWater) * 0.5f) + 0.5f;
	if (m_Water > 0)
		a_debugger.AddCube(m_Position, m_Size, m_Colour);
}


void Well::AddWater(float a_water)
{
	m_Water = glm::clamp(a_water + m_Water, 0.0f, m_MaxWater);
}
float Well::TakeWater(float a_water)
{
	if (m_Water == 0 || a_water == 0 || m_IsBuilding)
		return 0;

	m_Water -= a_water;
	float waterReturn = a_water;
	if (m_Water < 0)
	{
		waterReturn = a_water - m_Water;
		m_Water = 0;
	}
	return waterReturn;
}