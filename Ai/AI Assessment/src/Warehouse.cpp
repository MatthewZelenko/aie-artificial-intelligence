#include "Warehouse.h"

Warehouse::Warehouse() : Site("Warehouse")
{
	m_Food = 0;
	m_Water = 0;
	m_Wood = 0;
	m_Colour = glm::vec4(1.0f, 0.7f, 0.2f, 0.85f);
	m_Size = glm::vec3(40, 60, 80);
}
Warehouse::Warehouse(float a_food, float a_water, int a_wood) : Site("Warehouse")
{
	m_Food = a_food;
	m_Water = a_water;
	m_Wood = a_wood;
	m_Colour = glm::vec4(1.0f, 0.7f, 0.2f, 0.85f);
	m_Size = glm::vec3(40, 60, 80);
}
Warehouse::~Warehouse()
{
}

void Warehouse::Update(float a_deltaTime)
{

}
void Warehouse::Draw(Debugger& a_debugger)
{
	a_debugger.AddCube(m_Position, m_Size, m_Colour);
}

float Warehouse::TakeFood(float a_food)
{
	if (m_Food == 0 || a_food == 0)
		return 0;

	m_Food -= a_food;
	float foodReturn = a_food;
	if (m_Food < 0)
	{
		foodReturn = a_food + m_Food;
		m_Food = 0;
	}
	return foodReturn;
}
float Warehouse::TakeWater(float a_water)
{
	if (m_Water == 0 || a_water == 0)
		return 0;

	m_Water -= a_water;
	float waterReturn = a_water;
	if (m_Water < 0)
	{
		waterReturn = a_water + m_Water;
		m_Water = 0;
	}
	return waterReturn;
}
int Warehouse::TakeWood(int a_wood)
{
	if (m_Wood == 0 || a_wood == 0)
		return 0;

	m_Wood -= a_wood;
	int woodReturn = a_wood;
	if (m_Wood < 0)
	{
		woodReturn = a_wood + m_Wood;
		m_Wood = 0;
	}
	return woodReturn;
}

void Warehouse::AddFood(float a_food)
{
	m_Food += a_food;
}
void Warehouse::AddWater(float a_water)
{
	m_Water += a_water;	
}
void Warehouse::AddWood(int a_wood)
{
	m_Wood += a_wood;
}