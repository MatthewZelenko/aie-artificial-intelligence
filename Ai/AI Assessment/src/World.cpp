#include "World.h"
#include <cstdlib>
#include <GLFW\glfw3.h>

World::World()
{

}
World::~World()
{
	for (auto iter : m_Farmers)
	{
		iter.Destroy();
	}
	for (auto iter : m_WellCollectors)
	{
		iter.Destroy();
	}
	for (auto iter : m_Builders)
	{
		iter.Destroy();
	}
	for (auto iter : m_Lumberjacks)
	{
		iter.Destroy();
	}
}

void World::Init(const Camera& a_camera, Terrain& a_terrain)
{
	m_Warehouse = Warehouse(200, 200, 20);
	//Scene
	SetWareHousePosition(a_terrain, glm::vec2(0));
	CreateForest(a_terrain, glm::vec2(300, 300), 15, 50);
	CreateFarmField(a_terrain, glm::vec2(100, -200), 4);
	CreateEstate(a_terrain, glm::vec2(-300, 200), 6);
	CreateWellField(a_terrain, glm::vec2(400, -100), 5);

	//Pawns
	SpawnPawn(glm::vec3(), 1, "Farmer");
	SpawnPawn(glm::vec3(), 1, "WellCollector");
	SpawnPawn(glm::vec3(), 1, "Lumberjack");
	SpawnPawn(glm::vec3(), 1, "Builder");
	SpawnPawn(glm::vec3(), 2);
}
void World::Update(float a_deltaTime, Terrain& a_terrain)
{
	m_Warehouse.Update(a_deltaTime);
	for (std::vector<Farmer>::iterator farmer = m_Farmers.begin(); farmer != m_Farmers.end(); ++farmer)
	{
		if (!farmer->IsDead())
		{
			farmer->Update(a_deltaTime);
			if (farmer->IsDead())
			{
				farmer->SetColour(farmer->GetColour() * 0.25f);
				farmer->LeaveHome();
				farmer->GetFarm()->SetIsOccupied(false);
			}
			glm::vec3 position = farmer->GetPosition();
			farmer->SetYPosition(a_terrain.GetLandHeightFromPosition(position.x, position.z, true));
		}
	}
	for (std::vector<WellCollector>::iterator wellCollector = m_WellCollectors.begin(); wellCollector != m_WellCollectors.end(); ++wellCollector)
	{
		if (!wellCollector->IsDead())
		{
			wellCollector->Update(a_deltaTime);
			if (wellCollector->IsDead())
			{
				wellCollector->SetColour(wellCollector->GetColour() * 0.25f);
				wellCollector->GetWell()->SetIsOccupied(false);
			}
			glm::vec3 position = wellCollector->GetPosition();
			wellCollector->SetYPosition(a_terrain.GetLandHeightFromPosition(position.x, position.z, true));
		}
	}
	for (std::vector<Lumberjack>::iterator lumberjack = m_Lumberjacks.begin(); lumberjack != m_Lumberjacks.end(); ++lumberjack)
	{
		if (!lumberjack->IsDead())
		{
			lumberjack->Update(a_deltaTime);
			if (lumberjack->IsDead())
			{
				lumberjack->SetColour(lumberjack->GetColour() * 0.25f);
				lumberjack->GetTree()->SetIsOccupied(false);
			}
			glm::vec3 position = lumberjack->GetPosition();
			lumberjack->SetYPosition(a_terrain.GetLandHeightFromPosition(position.x, position.z, true));
		}
	}
	for (std::vector<Builder>::iterator builder = m_Builders.begin(); builder != m_Builders.end(); ++builder)
	{
		if (!builder->IsDead())
		{
			builder->Update(a_deltaTime);
			if (builder->IsDead())
			{
				builder->SetColour(builder->GetColour() * 0.25f);
				builder->GetConstructionSite()->SetIsOccupied(false);
			}
			glm::vec3 position = builder->GetPosition();
			builder->SetYPosition(a_terrain.GetLandHeightFromPosition(position.x, position.z, true));
		}
	}
	for (std::vector<House>::iterator house = m_Houses.begin(); house != m_Houses.end(); ++house)
	{
		if (house->ShouldSpawn())
		{
			house->SetShouldSpawn(false);
			SpawnPawn(house->GetPosition(), 2);
		}
	}
}
void World::Draw(Debugger& a_debugger, const Camera& a_camera)
{
	for (std::vector<Farmer>::iterator farmer = m_Farmers.begin(); farmer != m_Farmers.end(); ++farmer)
	{
		farmer->Draw(a_debugger);
	}
	for (std::vector<WellCollector>::iterator wellCollector = m_WellCollectors.begin(); wellCollector != m_WellCollectors.end(); ++wellCollector)
	{
		wellCollector->Draw(a_debugger);
	}
	for (std::vector<Lumberjack>::iterator lumberjack = m_Lumberjacks.begin(); lumberjack != m_Lumberjacks.end(); ++lumberjack)
	{
		lumberjack->Draw(a_debugger);
	}
	for (std::vector<Builder>::iterator builder = m_Builders.begin(); builder != m_Builders.end(); ++builder)
	{
		builder->Draw(a_debugger);
	}

	for (std::vector<Farm>::iterator farm = m_Farms.begin(); farm != m_Farms.end(); ++farm)
	{
		farm->Draw(a_debugger);
	}
	for (std::vector<Well>::iterator well = m_Wells.begin(); well != m_Wells.end(); ++well)
	{
		well->Draw(a_debugger);
	}
	for (std::vector<Tree>::iterator tree = m_Trees.begin(); tree != m_Trees.end(); ++ tree)
	{
		tree->Draw(a_debugger);
	}	
	
	for (std::vector<House>::iterator house = m_Houses.begin(); house != m_Houses.end(); ++house)
	{
		house->Draw(a_debugger);
	}
	m_Warehouse.Draw(a_debugger);
}

void World::CreateForest(Terrain& a_terrain, glm::vec2& a_position, int a_size, int a_chance)
{
	float halfSize = a_size * 0.5f;
	float treeSpread = 15;

	for (float z = -halfSize; z < halfSize; ++z)
	{
		for (float x = -halfSize; x < halfSize; ++x)
		{
			int random = rand() % 100 + 1;
			if (random % (100 / a_chance) == 0)
			{
				Tree tree(12);

				glm::vec3 size = tree.GetSize();

				int randomOffsetX = rand() % 15;
				int randomOffsetY = rand() % 15 - 7;

				float treeX = ((x * (size.x + treeSpread)) + ((size.x + treeSpread) * 0.5f)) + a_position.x + randomOffsetX;
				float treeZ = ((z * (size.z + treeSpread)) + ((size.z+ treeSpread) * 0.5f)) + a_position.y + randomOffsetY;
				tree.SetPosition(glm::vec3(treeX, a_terrain.GetLandHeightFromPosition(treeX, treeZ, true), treeZ));
				m_Trees.push_back(tree);
			}
		}
	}
}
void World::CreateFarmField(Terrain& a_terrain, glm::vec2& a_position, int a_size)
{
	float halfSize = a_size * 0.5f;
	float farmsSpread = 15;

	for (float z = -halfSize; z < halfSize; ++z)
	{
		for (float x = -halfSize; x < halfSize; ++x)
		{
				Farm farm(100);

				glm::vec3 size = farm.GetSize();

				float farmX = ((x * (size.x + farmsSpread)) + ((size.x + farmsSpread) * 0.5f)) + a_position.x;
				float farmZ = ((z * (size.z + farmsSpread)) + ((size.z + farmsSpread) * 0.5f)) + a_position.y;
				
				farm.SetPosition(glm::vec3(farmX, a_terrain.GetLandHeightFromPosition(farmX, farmZ, true), farmZ));
				m_Farms.push_back(farm);
		}
	}
}
void World::CreateEstate(Terrain& a_terrain, glm::vec2& a_position, int a_size, int a_chance)
{
	float halfSize = a_size * 0.5f;
	float houseSize = 30;
	float houseSpread = 20;

	for (float z = -halfSize; z < halfSize; ++z)
	{
		for (float x = -halfSize; x < halfSize; ++x)
		{
			int random = rand() % 100 + 1;
			if (random % (100 / a_chance) == 0)
			{
				House house(4, 10);

				int randomOffsetX = rand() % 21;
				int randomOffsetY = rand() % 21 - 10;

				glm::vec3 size = house.GetSize();

				float treeX = ((x * (size.x + houseSpread)) + ((size.x + houseSpread) * 0.5f)) + a_position.x + randomOffsetX;
				float treeZ = ((z * (size.z + houseSpread)) + ((size.z + houseSpread) * 0.5f)) + a_position.y + randomOffsetY;
				house.SetPosition(glm::vec3(treeX, a_terrain.GetLandHeightFromPosition(treeX, treeZ, true), treeZ));
				m_Houses.push_back(house);
			}
		}
	}
}
void World::CreateWellField(Terrain& a_terrain, glm::vec2& a_position, int a_size, int chance)
{
	float halfSize = a_size * 0.5f;
	float wellSpread = 15;

	for (float z = -halfSize; z < halfSize; ++z)
	{
		for (float x = -halfSize; x < halfSize; ++x)
		{
			Well well(10000);

			glm::vec3 size = well.GetSize();

			float wellX = ((x * (size.x + wellSpread)) + ((size.x + wellSpread) * 0.5f)) + a_position.x;
			float wellZ = ((z * (size.z + wellSpread)) + ((size.z + wellSpread) * 0.5f)) + a_position.y;

			well.SetPosition(glm::vec3(wellX, a_terrain.GetLandHeightFromPosition(wellX, wellZ, true), wellZ));
			m_Wells.push_back(well);
		}
	}
}
void World::SetWareHousePosition(Terrain& a_terrain, glm::vec2& a_position)
{
	m_Warehouse.SetPosition(glm::vec3(a_position.x, a_terrain.GetLandHeightFromPosition(a_position.x, a_position.y, true), a_position.y));

}

Tree* World::GetNearestTree(glm::vec3& a_position, bool a_setOccupied)
{
	Tree* closest = nullptr;
	float minDistance = (float)INT_MAX;
	for (std::vector<Tree>::iterator tree = m_Trees.begin(); tree != m_Trees.end(); ++tree)
	{
		if (tree->IsOccupied())
			continue;
		float distance = glm::length(tree->GetPosition() - a_position);
		if (distance <= minDistance)
		{
			minDistance = distance;
			closest = &(*tree);
		}
	}
	if (closest && a_setOccupied)
		closest->SetIsOccupied(true);
	return closest;
}
Farm* World::GetNearestFarm(glm::vec3& a_position, bool a_setOccupied)
{
	Farm* closest = nullptr;
	float minDistance = (float)INT_MAX;
	for (std::vector<Farm>::iterator farm = m_Farms.begin(); farm != m_Farms.end(); ++farm)
	{
		if (farm->IsOccupied())
			continue;
		float distance = glm::length(farm->GetPosition() - a_position);
		if (distance <= minDistance)
		{
			minDistance = distance;
			closest = &(*farm);
		}
	}
	if (closest && a_setOccupied)
		closest->SetIsOccupied(true);
	return closest;
}
Well* World::GetNearestWell(glm::vec3& a_position)
{
	Well* closest = nullptr;
	float minDistance = (float)INT_MAX;
	for (std::vector<Well>::iterator well = m_Wells.begin(); well != m_Wells.end(); ++well)
	{
		if (well->GetWater() <= 0.0f && !well->IsBuilding())
		{
			continue;
		}
		float distance = glm::length(well->GetPosition() - a_position);
		if (distance <= minDistance)
		{
			minDistance = distance;
			closest = &(*well);
		}
	}
	return closest;
}
House* World::GetNearestHouse(glm::vec3& a_position, bool a_lookForBuilt, bool a_lookForFull)
{
	House* closest = nullptr;
	float minDistance = (float)INT_MAX;
	for (std::vector<House>::iterator house = m_Houses.begin(); house != m_Houses.end(); ++house)
	{
		if (a_lookForBuilt)
		{
			if (!house->IsBuilt() && (a_lookForFull && !house->IsFull() || !a_lookForFull && house->IsFull()))
				continue;
		}
		else
		{
			if (house->IsBuilt() || house->IsOccupied())
				continue;
		}

		float distance = glm::length(house->GetPosition() - a_position);
		if (distance <= minDistance)
		{
			minDistance = distance;
			closest = &(*house);
		}
	}
	if (closest && !a_lookForBuilt)
		closest->SetIsOccupied(true);
	return closest;
}

void World::SpawnPawn(glm::vec3& a_position, int a_number, std::string a_type)
{
	for (int i = 0; i < a_number; ++i)
	{
		std::string type = a_type;
		if (type == "")
		{
			int rng = rand() % 4;

			if (rng == 0)
			{
				type = "Farmer";
			}
			else if (rng == 1)
			{
				type = "WellCollector";
			}
			else if (rng == 2)
			{
				type = "Lumberjack";
			}
			else 
			{
				type = "Builder";
			}
		}	

		if (type == "Farmer")
		{
			int size = rand() % 4 + 2;
			int height = rand() % 4 + 8;

			Farmer farmer;
			farmer.Init(this, (float)size * 12.0f, 50, 100, 200, 60.0f);
			farmer.SetSize(glm::vec3(size, height, size));
			farmer.SetPosition(a_position);

			m_Farmers.push_back(farmer);
		}
		else if (type == "WellCollector")
		{
			int size = rand() % 2 + 2;
			int height = rand() % 4 + 8;

			WellCollector wellCollector;
			wellCollector.Init(this, (float)size * 60.0f, 50, 100, 200, 60.0f);
			wellCollector.SetSize(glm::vec3(size, height, size));
			wellCollector.SetPosition(a_position);

			m_WellCollectors.push_back(wellCollector);
		}
		else if (type == "Lumberjack")
		{
			int size = rand() % 4 + 4;
			int height = rand() % 4 + 12;

			Lumberjack lumberjack;
			lumberjack.Init(this, size, 50, 100, 200, 60.0f);
			lumberjack.SetSize(glm::vec3(size, height, size));
			lumberjack.SetPosition(a_position);

			m_Lumberjacks.push_back(lumberjack);
		}
		else if (type == "Builder")
		{
			int size = rand() % 4 + 2;
			int height = rand() % 4 + 12;

			Builder builder;
			builder.Init(this, size, 50, 100, 200, 60.0f);
			builder.SetSize(glm::vec3(size, height, size));
			builder.SetPosition(a_position);

			m_Builders.push_back(builder);
		}
	}
}