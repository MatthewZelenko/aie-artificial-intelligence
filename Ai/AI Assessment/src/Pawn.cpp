#include "Pawn.h"
#include "World.h"

Pawn::Pawn(std::string a_job)
{
	m_World = nullptr;
	m_Home = nullptr;

	m_DecreaseElapsed = 0;
	m_DecreaseRate = 2;
	m_FoodDecreaseRate = 1;
	m_WaterDecreaseRate = 4;
	m_RestDecreaseRate = 10;

	m_Food = 40;
	m_Water = 200;
	m_Rest = 400;
	m_MaxFood = 40;
	m_MaxWater = 200;
	m_MaxWater = 400;

	m_IsDead = false;
	m_IsBusy = false;

	m_Size = glm::vec3(5, 10, 5);
	m_Speed = 0;
	m_Job = a_job;
}
Pawn::~Pawn()
{

}


void Pawn::Destroy()
{
	m_EatModule.Destroy();
	m_DrinkModule.Destroy();
	m_RestModule.Destroy();
}

void Pawn::Init(World* a_world, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed)
{
	m_World = a_world;
	m_MaxFood = a_maxFood;
	m_Food = a_maxFood;
	m_MaxWater = a_maxWater;
	m_Water = a_maxWater;
	m_MaxRest = a_maxRest;
	m_Rest = a_maxRest;
	m_Speed = a_speed;
	CreateEatModule();
	CreateDrinkModule();
	CreateRestModule();
	CreateBaseRules();
}
void Pawn::Draw(Debugger& a_debugger)
{
	a_debugger.AddCube(m_Position + glm::vec3(0.0f, m_Size.y * 0.5f, 0.0f), m_Size, m_Colour);
}

void Pawn::CreateEatModule()
{
	float x0, x1, x2, x3;
	x0 = m_MaxFood * 0.25f;
	x1 = m_MaxFood * 0.5f;
	x2 = m_MaxFood * 0.75f;
	x3 = m_MaxFood;
	Fuzzy::Set& hunger = m_EatModule.CreateSet("Hunger");
	hunger.AddMember(new Fuzzy::LeftShoulder("Starving", x0, x1));
	hunger.AddMember(new Fuzzy::Triangle("Hungry", x0, x1, x2));
	hunger.AddMember(new Fuzzy::RightShoulder("Full", x1, x2, x3));

	Fuzzy::Set& distance = m_EatModule.CreateSet("Distance");
	distance.AddMember(new Fuzzy::LeftShoulder("Close", 50, 100));
	distance.AddMember(new Fuzzy::Triangle("Medium", 50, 100, 200));
	distance.AddMember(new Fuzzy::RightShoulder("Far", 100, 200, 400));

	Fuzzy::Set& desirability = m_EatModule.CreateSet("Desirability");
	desirability.AddMember(new Fuzzy::LeftShoulder("Undesirable", 25, 50));
	desirability.AddMember(new Fuzzy::Triangle("Tolerable", 25, 50, 75));
	desirability.AddMember(new Fuzzy::RightShoulder("Desirable", 50, 75, 100));
}
void Pawn::CreateDrinkModule()
{
	float x0, x1, x2, x3;
	x0 = m_MaxWater * 0.25f;
	x1 = m_MaxWater * 0.5f;
	x2 = m_MaxWater * 0.75f;
	x3 = m_MaxWater;

	Fuzzy::Set& thirst = m_DrinkModule.CreateSet("Thirst");
	thirst.AddMember(new Fuzzy::LeftShoulder("Dehydrated", x0, x1));
	thirst.AddMember(new Fuzzy::Triangle("Thirsty", x0, x1, x2));
	thirst.AddMember(new Fuzzy::RightShoulder("Hydrated", x1, x2, x3));

	Fuzzy::Set& distance = m_DrinkModule.CreateSet("Distance");
	distance.AddMember(new Fuzzy::LeftShoulder("Close", 50, 100));
	distance.AddMember(new Fuzzy::Triangle("Medium", 50, 100, 200));
	distance.AddMember(new Fuzzy::RightShoulder("Far", 100, 200, 400));

	Fuzzy::Set& desirability = m_DrinkModule.CreateSet("Desirability");
	desirability.AddMember(new Fuzzy::LeftShoulder("Undesirable", 25, 50));
	desirability.AddMember(new Fuzzy::Triangle("Tolerable", 25, 50, 75));
	desirability.AddMember(new Fuzzy::RightShoulder("Desirable", 50, 75, 100));
}
void Pawn::CreateRestModule()
{
	float x0, x1, x2, x3;
	x0 = m_MaxRest * 0.25f;
	x1 = m_MaxRest * 0.5f;
	x2 = m_MaxRest * 0.75f;
	x3 = m_MaxRest;

	Fuzzy::Set& tiredness = m_RestModule.CreateSet("Tiredness");
	tiredness.AddMember(new Fuzzy::LeftShoulder("Sleepy", x0, x1));
	tiredness.AddMember(new Fuzzy::Triangle("Tired", x0, x1, x2));
	tiredness.AddMember(new Fuzzy::RightShoulder("Awake", x1, x2, x3));

	Fuzzy::Set& distance = m_RestModule.CreateSet("Distance");
	distance.AddMember(new Fuzzy::LeftShoulder("Close", 50, 100));
	distance.AddMember(new Fuzzy::Triangle("Medium", 50, 100, 200));
	distance.AddMember(new Fuzzy::RightShoulder("Far", 100, 200, 400));

	Fuzzy::Set& desirability = m_RestModule.CreateSet("Desirability");
	desirability.AddMember(new Fuzzy::LeftShoulder("Undesirable", 25, 50));
	desirability.AddMember(new Fuzzy::Triangle("Tolerable", 25, 50, 75));
	desirability.AddMember(new Fuzzy::RightShoulder("Desirable", 50, 75, 100));
}
void Pawn::CreateBaseRules()
{
	m_EatModule.AddRule(m_EatModule["Hunger"]["Starving"], m_EatModule["Desirability"]["Desirable"]);
	m_EatModule.AddRule(new Fuzzy::And(m_EatModule["Hunger"]["Hungry"], m_EatModule["Distance"]["Close"]), m_EatModule["Desirability"]["Desirable"]);
	m_EatModule.AddRule(new Fuzzy::And(m_EatModule["Hunger"]["Hungry"], m_EatModule["Distance"]["Medium"]), m_EatModule["Desirability"]["Tolerable"]);
	m_EatModule.AddRule(new Fuzzy::And(m_EatModule["Hunger"]["Hungry"], m_EatModule["Distance"]["Far"]), m_EatModule["Desirability"]["Undesirable"]);
	m_EatModule.AddRule(m_EatModule["Hunger"]["Full"], m_EatModule["Desirability"]["Undesirable"]);

	m_DrinkModule.AddRule(m_DrinkModule["Thirst"]["Dehydrated"], m_DrinkModule["Desirability"]["Desirable"]);
	m_DrinkModule.AddRule(new Fuzzy::And(m_DrinkModule["Thirst"]["Thirsty"], m_DrinkModule["Distance"]["Close"]), m_DrinkModule["Desirability"]["Desirable"]);
	m_DrinkModule.AddRule(new Fuzzy::And(m_DrinkModule["Thirst"]["Thirsty"], m_DrinkModule["Distance"]["Medium"]), m_DrinkModule["Desirability"]["Tolerable"]);
	m_DrinkModule.AddRule(new Fuzzy::And(m_DrinkModule["Thirst"]["Thirsty"], m_DrinkModule["Distance"]["Far"]), m_DrinkModule["Desirability"]["Undesirable"]);
	m_DrinkModule.AddRule(m_DrinkModule["Thirst"]["Hydrated"], m_DrinkModule["Desirability"]["Undesirable"]);

	m_RestModule.AddRule(m_RestModule["Tiredness"]["Sleepy"], m_RestModule["Desirability"]["Desirable"]);
	m_RestModule.AddRule(new Fuzzy::And(m_RestModule["Tiredness"]["Tired"], m_RestModule["Distance"]["Close"]), m_RestModule["Desirability"]["Desirable"]);
	m_RestModule.AddRule(new Fuzzy::And(m_RestModule["Tiredness"]["Tired"], m_RestModule["Distance"]["Medium"]), m_RestModule["Desirability"]["Tolerable"]);
	m_RestModule.AddRule(new Fuzzy::And(m_RestModule["Tiredness"]["Tired"], m_RestModule["Distance"]["Far"]), m_RestModule["Desirability"]["Undesirable"]);
	m_RestModule.AddRule(m_RestModule["Tiredness"]["Awake"], m_RestModule["Desirability"]["Undesirable"]);
}
void Pawn::DecreaseStats(float a_deltaTime)
{
	m_DecreaseElapsed += a_deltaTime;
	if (m_DecreaseElapsed >= m_DecreaseRate)
	{
		m_Food -= m_FoodDecreaseRate;
		m_Water -= m_WaterDecreaseRate;
		m_Rest -= m_RestDecreaseRate;
		m_DecreaseElapsed = 0.0f;
		if (m_Food <= 0.0f)
		{
			std::printf("A %s died from Starving!", m_Job.c_str());
			m_IsDead = true;
		}
		if (m_Water <= 0.0f)
		{
			std::printf("A %s died from Dehydration!", m_Job.c_str());
			m_IsDead = true;
		}
		if (m_Rest <= 0.0f)
		{
			std::printf("A %s died from no Sleep!", m_Job.c_str());
			m_IsDead = true;
		}
	}
}
void Pawn::SelectAction()
{
	if (!m_IsBusy)
	{
		float highestValue = 0.0f;
		m_Action = "";

		float eatValue = EvaluateHunger();
		if (eatValue > highestValue)
		{
			m_Action = "Eat";
			highestValue = eatValue;
		}

		float drinkValue = EvaluateThirst();
		if (drinkValue > highestValue)
		{
			m_Action = "Drink";
			highestValue = drinkValue;
		}

		float restValue = EvaluateSleep();
		if (restValue > highestValue)
		{
			m_Action = "Rest";
			highestValue = restValue;
		}
	}
}
float Pawn::EvaluateHunger()
{
	m_EatModule["Hunger"].CalculateDOM(m_Food);
	m_EatModule["Distance"].CalculateDOM(glm::length(m_World->GetWarehouse()->GetPosition() - m_Position));
	return m_EatModule.Defuzzify(Fuzzy::CENTROID, m_EatModule["Desirability"]);
}
float Pawn::EvaluateThirst()
{
	m_DrinkModule["Thirst"].CalculateDOM(m_Water);
	m_DrinkModule["Distance"].CalculateDOM(glm::length(m_World->GetWarehouse()->GetPosition() - m_Position));
	return m_DrinkModule.Defuzzify(Fuzzy::CENTROID, m_DrinkModule["Desirability"]);
}
float Pawn::EvaluateSleep()
{
	if (!m_Home)
	{
		m_Home = m_World->GetNearestHouse(m_Position, true);
		if(m_Home)
			m_Home->AddResidents(1);
	}
	if (m_Home)
	{
		m_RestModule["Tiredness"].CalculateDOM(m_Rest);
		m_RestModule["Distance"].CalculateDOM(glm::length(m_Home->GetPosition() - m_Position));
		return m_RestModule.Defuzzify(Fuzzy::CENTROID, m_RestModule["Desirability"]);
	}
	return 0.0f;
}
void Pawn::DoAction(float a_deltaTime)
{
	//If action is eat
	if (m_Action == "Eat")
	{
		//Get warehouse
		Warehouse* warehouse = m_World->GetWarehouse();
		glm::vec3 direction = warehouse->GetPosition() - m_Position;
		float distance = glm::length(direction);
		direction = glm::normalize(direction);
		//If distance to warehouse is greater than 0 move towards warehouse
		if (distance > 0)
		{
			float speed = a_deltaTime * m_Speed;
			//if speed is greater than distance then just jump to warehouse
			if (speed >= distance)
			{
				m_Position = warehouse->GetPosition();
			}
			//else move towards it normally
			else
			{
				m_Position += direction * speed;
			}
		}
		//else if pawn is at warehouse
		else
		{
			//how much food can we take?
			float food = warehouse->TakeFood(m_MaxFood - glm::max(m_Food, 0.0f));
			//Bon Apetite'
			m_Food += food;
		}
	}
	if (m_Action == "Drink")
	{
		//Get warehouse
		Warehouse* warehouse = m_World->GetWarehouse();
		glm::vec3 direction = warehouse->GetPosition() - m_Position;
		float distance = glm::length(direction);
		direction = glm::normalize(direction);
		//If distance to warehouse is greater than 0 move towards warehouse
		if (distance > 0)
		{
			float speed = a_deltaTime * m_Speed;
			//if speed is greater than distance then just jump to warehouse
			if (speed >= distance)
			{
				m_Position = warehouse->GetPosition();
			}
			//else move towards it normally
			else
			{
				m_Position += direction * speed;
			}
		}
		//else if pawn is at warehouse
		else
		{
			//how much food can we take?
			float water = warehouse->TakeWater(m_MaxWater - glm::max(m_Water, 0.0f));
			//Bon Apetite'
			m_Water += water;
		}
	}
	if (m_Action == "Rest")
	{
		if (m_Home)
		{
			glm::vec3 housePosition = m_Home->GetPosition();
			glm::vec3 direction = housePosition - m_Position;
			float distance = glm::length(direction);
			direction = glm::normalize(direction);
			//If distance to house is greater than 0 move towards house
			if (distance > 0)
			{
				float speed = a_deltaTime * m_Speed;
				//if speed is greater than distance then just jump to house
				if (speed >= distance)
				{
					m_Position = housePosition;
				}
				//else move towards it normally
				else
				{
					m_Position += direction * speed;
				}
			}
			//else pawn is at home and rest
			else
			{
				m_Rest = m_MaxRest;
			}
		}
	}
}

bool Pawn::SetHome(House* a_house)
{
	if (a_house->AddResidents(1))
	{
		m_Home = a_house;
		return true;
	}
	return false;
}
void Pawn::LeaveHome()
{
	if (m_Home)
	{
		m_Home->AddResidents(-1);
	}
}

void Pawn::SetPosition(glm::vec3& a_position)
{
	m_Position = a_position;
}
void Pawn::SetSize(glm::vec3& a_size)
{
	m_Size = a_size;
}
void Pawn::SetYPosition(float a_y)
{
	m_Position.y = a_y;
}
void Pawn::SetColour(glm::vec4& a_colour)
{
	m_Colour = a_colour;
}
void Pawn::SetJob(std::string a_title)
{
	m_Job = a_title;
}
void Pawn::SetFood(float a_food)
{
	m_Food = a_food;
}
void Pawn::SetWater(float a_water)
{
	m_Water = a_water;
}
void Pawn::SetRest(float a_rest)
{
	m_Rest = a_rest;
}

void Pawn::AddPosition(glm::vec3& a_position)
{
	m_Position += a_position;
}