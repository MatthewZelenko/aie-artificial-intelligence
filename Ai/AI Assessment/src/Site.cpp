#include "Site.h"

Site::Site(std::string a_name) : m_Name(a_name), m_Bound(nullptr), m_Colour(1.0f), m_IsOccupied(false)
{
}
Site::~Site()
{
	delete m_Bound;
}

void Site::SetPosition(glm::vec3& a_position)
{
	m_Position = a_position;
}
void Site::SetSize(glm::vec3& a_size)
{
	m_Size = a_size;
}
void Site::SetBound(BoundingObject* a_bound)
{
	m_Bound = a_bound;
}
void Site::SetName(std::string& a_name)
{
	m_Name = a_name;
}

void Site::AddPosition(glm::vec3& a_position)
{
	m_Position += a_position;
}

void Site::SetColour(glm::vec4& a_colour)
{
	m_Colour = a_colour;
}
void Site::SetIsOccupied(bool a_value)
{
	m_IsOccupied = a_value;
}