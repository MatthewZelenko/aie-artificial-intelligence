#include "Lumberjack.h"
#include "World.h"
#include "Tree.h"

Lumberjack::Lumberjack() : Pawn("Lumberjack")
{
	m_CollectedWood = 0;
	m_Colour = glm::vec4(1.0f, 0.3f, 0.2f, 1.0f);
	m_Tree = nullptr;
}
Lumberjack::~Lumberjack()
{

}

void Lumberjack::Init(World* a_world, int a_maxCollectedWood, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed)
{
	Pawn::Init(a_world, a_maxFood, a_maxWater, a_maxRest, a_speed);
	m_MaxCollectedWood = a_maxCollectedWood;
}
void Lumberjack::Update(float a_deltaTime)
{
	DecreaseStats(a_deltaTime);
	SelectAction();
	DoAction(a_deltaTime);
}

void Lumberjack::SelectAction()
{
	if (!m_IsBusy)
	{
		float highestValue = 0.0f;
		m_Action = "";

		float eatValue = EvaluateHunger();
		if (eatValue > highestValue)
		{
			m_Action = "Eat";
			highestValue = eatValue;
		}

		float drinkValue = EvaluateThirst();
		if (drinkValue > highestValue)
		{
			m_Action = "Drink";
			highestValue = drinkValue;
		}

		float restValue = EvaluateSleep();
		if (restValue > highestValue)
		{
			m_Action = "Rest";
			highestValue = restValue;
		}

		float treeValue = 80.0f;
		if (treeValue > highestValue)
		{
			m_Action = "CollectWood";
			highestValue = treeValue;
		}
	}
}
void Lumberjack::DoAction(float a_deltaTime)
{
	Pawn::DoAction(a_deltaTime);
	if (m_Action == "CollectWood")
	{
		//If empty handed
		if (m_CollectedWood == 0)
		{
			//Get well
			if (!m_Tree || m_Tree->GetWood() <= 0)
				m_Tree = m_World->GetNearestTree(m_Position, true);
			if (m_Tree)
			{
				glm::vec3 direction = m_Tree->GetPosition() - m_Position;
				float distance = glm::length(direction);
				direction = glm::normalize(direction);
				//If distance to well is greater than 0 move towards well
				if (distance > 0)
				{
					float speed = a_deltaTime * m_Speed;
					//if speed is greater than distance then just jump to well
					if (speed >= distance)
					{
						m_Position = m_Tree->GetPosition();
					}
					//else move towards it normally
					else
					{
						m_Position += direction * speed;
					}
				}
				//else if pawn is at well
				else
				{
					//how much Wood can we take?
					int Wood = m_Tree->TakeWood(m_MaxCollectedWood - glm::max(m_CollectedWood, 0));
					//Bon Apetite'
					m_CollectedWood += Wood;
				}
			}
		}
		//else
		else
		{
			//Get warehouse
			Warehouse* warehouse = m_World->GetWarehouse();
			glm::vec3 direction = warehouse->GetPosition() - m_Position;
			float distance = glm::length(direction);
			direction = glm::normalize(direction);
			//If distance to warehouse is greater than 0 move towards warehouse
			if (distance > 0)
			{
				float speed = a_deltaTime * m_Speed;
				//if speed is greater than distance then just jump to warehouse
				if (speed >= distance)
				{
					m_Position = warehouse->GetPosition();
				}
				//else move towards it normally
				else
				{
					m_Position += direction * speed;
				}
			}
			//else if pawn is at warehouse
			else
			{
				//give food
				warehouse->AddWood(m_CollectedWood);
				m_CollectedWood = 0;
			}
		}
	}
}

void Lumberjack::SetMaxCollectedWood(int a_max)
{
	m_MaxCollectedWood = a_max;
}
void Lumberjack::SetCollectedWood(int a_value)
{
	m_CollectedWood = glm::clamp(a_value, 0, m_MaxCollectedWood);
}

void Lumberjack::AddCollectedWood(int a_value)
{
	m_CollectedWood = glm::clamp(m_CollectedWood + a_value, 0, m_MaxCollectedWood);
}