#include "Tree.h"

Tree::Tree(int a_wood) : Site("Tree")
{
	m_Wood = a_wood;
	m_MaxWood = a_wood;
	m_Colour = glm::vec4(0.6f, 0.3f, 0.1f, 1.0f);
	m_Size = glm::vec3(5, 100, 5);
}
Tree::~Tree()
{
}

void Tree::Update(float a_deltaTime)
{

}
void Tree::Draw(Debugger& a_debugger)
{
	m_Colour.a = (((float)m_Wood / (float)m_MaxWood) * 0.75f) + 0.25f;
	if(m_Wood > 0)
		a_debugger.AddCube(m_Position, m_Size, m_Colour);
}

int Tree::TakeWood(int a_wood)
{
	if (m_Wood == 0 || a_wood == 0)
		return 0;

	m_Wood -= a_wood;
	int woodReturn = a_wood;
	if (m_Wood < 0)
	{
		woodReturn = a_wood - m_Wood;
		m_Wood = 0;
	}
	return woodReturn;
}