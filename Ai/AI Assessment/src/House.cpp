#include "House.h"

House::House(int a_maxResidents, int a_requiredLogs) : Site("House")
{
	m_MaxResidents = a_maxResidents;
	m_Residents = 0;
	m_RequiredLogs = a_requiredLogs;
	m_IsBuilt = false;
	m_Colour = glm::vec4(1.0f, 0.2f, 0.2f, 1.0f);
	m_Size = glm::vec3(20, 30, 20);
	m_Logs = 0;
	m_SpawnPeople = false;
}
House::~House()
{

}

void House::Update(float a_deltaTime)
{
	
}
void House::Draw(Debugger& a_debugger)
{
	m_Colour.a = (((float)m_Logs / (float)m_RequiredLogs) * 0.9f) + 0.1f;
	a_debugger.AddCube(m_Position, m_Size, m_Colour);
}

int House::AddResidents(int a_number)
{
	int returnValue = 0;
	if (m_IsBuilt)
	{
		if (a_number > 0)
		{
			if (m_Residents < m_MaxResidents)
			{
				int oldResidents = m_Residents;
				m_Residents += a_number;
				if (m_Residents <= m_MaxResidents)
				{
					returnValue = a_number;
				}
				else
				{
					returnValue = m_MaxResidents - oldResidents;
					m_Residents = m_MaxResidents;
					m_IsFull = true;
				}
			}
		}
		else
		{
			m_Residents += a_number;
			m_IsFull = false;
			if (m_Residents < 0)
				m_Residents = 0;
		}
	}
	return returnValue;
}
void House::AddLog()
{
	if (!m_IsBuilt)
	{
		m_Logs++;
		if (m_Logs == m_RequiredLogs)
		{
			m_IsBuilt = true;
			m_SpawnPeople = true;
			m_IsOccupied = false;
		}
	}
}