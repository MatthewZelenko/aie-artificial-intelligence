#include "Farmer.h"
#include "World.h"
#include "Farm.h"

Farmer::Farmer() : Pawn("Farmer")
{
	m_CollectedFood = 0.0f;
	m_Colour = glm::vec4(0.75f, 0.4f, 0.0f, 1.0f);
	m_Farm = nullptr;
}
Farmer::~Farmer()
{

}

void Farmer::Init(World* a_world, float a_maxCollectedFood, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed)
{
	Pawn::Init(a_world, a_maxFood, a_maxWater, a_maxRest, a_speed);
	m_MaxCollectedFood = a_maxCollectedFood;
}
void Farmer::Update(float a_deltaTime)
{
	DecreaseStats(a_deltaTime);
	//std::system("cls");
	//std::printf("Food: %f\n", m_Food);
	//std::printf("Water: %f\n", m_Water);
	//std::printf("Collected Food: %f\n", m_CollectedFood);
	SelectAction();
	DoAction(a_deltaTime);
}

void Farmer::SelectAction()
{
	if (!m_IsBusy)
	{
		Warehouse* warehouse = m_World->GetWarehouse();
		float highestValue = 0.0f;
		m_Action = "";

		float farmValue = 80.0f;
		if (farmValue > highestValue)
		{
			m_Action = "Farm";
			highestValue = farmValue;
		}
		float eatValue = EvaluateHunger();
		if (eatValue > highestValue && warehouse->GetFood() > 0)
		{
			m_Action = "Eat";
			highestValue = eatValue;
		}

		float drinkValue = EvaluateThirst();
		if (drinkValue > highestValue)
		{
			m_Action = "Drink";
			highestValue = drinkValue;
		}

		float restValue = EvaluateSleep();
		if (restValue > highestValue)
		{
			m_Action = "Rest";
			highestValue = restValue;
		}

		
	}
}
void Farmer::DoAction(float a_deltaTime)
{
	Pawn::DoAction(a_deltaTime);
	if (m_Action == "Farm")
	{
		//If empty handed
		if (m_CollectedFood == 0)
		{
			//Get farm
			if(!m_Farm)
				m_Farm = m_World->GetNearestFarm(m_Position, true);
			if (m_Farm)
			{
				if (m_World->GetWarehouse()->GetFood() == 0)
				{
					m_IsBusy = true;
				}

				glm::vec3 direction = m_Farm->GetPosition() - m_Position;
				float distance = glm::length(direction);
				direction = glm::normalize(direction);
				//If distance to farm is greater than 0 move towards farm
				if (distance > 0)
				{
					float speed = a_deltaTime * m_Speed;
					//if speed is greater than distance then just jump to farm
					if (speed >= distance)
					{
						m_Position = m_Farm->GetPosition();
					}
					//else move towards it normally
					else
					{
						m_Position += direction * speed;
					}
				}
				//else if pawn is at farm
				else
				{
					if (m_Farm->IsGrowing())
					{
						m_Farm->AddFood(a_deltaTime * 10);
						if (m_Farm->GetFood() >= m_Farm->GetMaxFood())
						{
							m_Farm->SetIsGrowing(false);
						}
					}
					else
					{
						//how much food can we take?
						float food = m_Farm->TakeFood(m_MaxCollectedFood - glm::max(m_CollectedFood, 0.0f));
						//Bon Apetite'
						m_CollectedFood += food;
						if (m_Farm->GetFood() <= 0.0f)
						{
							m_Farm->SetIsGrowing(true);
						}
					}
				}
			}
		}
		//else
		else
		{
			m_IsBusy = true;
			//Get warehouse
			Warehouse* warehouse = m_World->GetWarehouse();
			glm::vec3 direction = warehouse->GetPosition() - m_Position;
			float distance = glm::length(direction);
			direction = glm::normalize(direction);
			//If distance to warehouse is greater than 0 move towards warehouse
			if (distance > 0)
			{
				float speed = a_deltaTime * m_Speed;
				//if speed is greater than distance then just jump to warehouse
				if (speed >= distance)
				{
					m_Position = warehouse->GetPosition();
				}
				//else move towards it normally
				else
				{
					m_Position += direction * speed;
				}
			}
			//else if pawn is at warehouse
			else
			{
				//give food
				warehouse->AddFood(m_CollectedFood);
				m_CollectedFood = 0.0f;
				m_IsBusy = false;
			}
		}
	}
}

void Farmer::SetMaxCollectedFood(float a_max)
{
	m_MaxCollectedFood = a_max;
}
void Farmer::SetCollectedFood(float a_value)
{
	m_CollectedFood = glm::clamp(a_value, 0.0f, m_MaxCollectedFood);
}

void Farmer::AddCollectedFood(float a_value)
{
	m_CollectedFood = glm::clamp(m_CollectedFood + a_value, 0.0f, m_MaxCollectedFood);
}