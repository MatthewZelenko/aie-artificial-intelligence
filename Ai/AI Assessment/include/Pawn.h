#pragma once
#include "Debugger.h"
#include "FuzzyModule.h"
#include "FuzzyMember.h"
#include "FuzzyOperator.h"

class World;
class House;

class Pawn
{
public:
	Pawn(std::string a_job);
	virtual ~Pawn();
	virtual void Destroy();

	virtual void Init(World* a_world, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed);
	virtual void Update(float a_deltaTime) = 0;
	void Draw(Debugger& a_debugger);

	/*Sets what house the pawn will live at.*/
	bool SetHome(House* a_house);
	/*Releases the pawn of his home, decrementing house residents, and setting IsFull bool to false.*/
	void LeaveHome();

	/*Sets the position.*/
	void SetPosition(glm::vec3& a_position);
	/*Sets just the y Position.*/
	void SetYPosition(float a_y);
	/*Sets the size of the cube body.*/
	void SetSize(glm::vec3& a_size);
	/*Sets the colour of the cube body.*/
	void SetColour(glm::vec4& a_colour);
	/*Sets the pawns job.*/
	void SetJob(std::string a_title);
	/*Sets the food value.*/
	void SetFood(float a_food);
	/*Sets the water value.*/
	void SetWater(float a_water);
	/*Sets the rest value.*/
	void SetRest(float a_rest);

	/*Increments the position.*/
	void AddPosition(glm::vec3& a_position);

	/*Returns the position.*/
	glm::vec3 GetPosition() { return m_Position; }
	/*Returns the size of the cube body.*/
	glm::vec3 GetSize() { return m_Size; }
	/*Returns the colour of the cube body.*/
	glm::vec4 GetColour() { return m_Colour; }
	/*Returns the job title.*/
	std::string	GetJob() { return m_Job; }
	/*Returns the food count.*/
	float GetFood() { return m_Food; }
	/*Returns the water count.*/
	float GetWater() { return m_Water; }
	/*Returns the rest count.*/
	float GetRest() { return m_Rest; }
	/*Returns if the pawn is dead.*/
	bool IsDead() { return m_IsDead; }
protected:
	/*Creates the Eat module.*/
	virtual void CreateEatModule();
	/*Creates the Drink module.*/
	virtual void CreateDrinkModule();
	/*Creates the Rest module.*/
	virtual void CreateRestModule();
	/*Creates the Eat, Drink, Rest modules rules.*/
	void CreateBaseRules();
	/*Decreases the stats over time bases on set parameters.*/
	virtual void DecreaseStats(float a_deltaTime);
	/*Runs through Modules to select best action. Virtual to account for inheritence classes to add more modules.*/
	virtual void SelectAction();
	/*Evaluates the Eat module.*/
	float EvaluateHunger();
	/*Evaluates the Drink module*/
	float EvaluateThirst();
	/*Evaluates the Rest module.*/
	float EvaluateSleep();
	/*First run SelectAction function to select an action. Then run this to do that action.*/
	virtual void DoAction(float a_deltaTime);

	World* m_World;
	 
	std::string m_Job;
	std::string m_Action;

	glm::vec3 m_Position;
	glm::vec3 m_Size;
	glm::vec4 m_Colour;
	float m_Speed;

	Fuzzy::Module m_EatModule;
	Fuzzy::Module m_DrinkModule;
	Fuzzy::Module m_RestModule;

	float m_DecreaseElapsed, m_DecreaseRate;
	float m_FoodDecreaseRate;
	float m_WaterDecreaseRate;
	float m_RestDecreaseRate;

	float m_Food, m_MaxFood;
	float m_Water, m_MaxWater;
	float m_Rest, m_MaxRest;

	bool m_IsDead;
	bool m_IsBusy;

	House* m_Home;
};