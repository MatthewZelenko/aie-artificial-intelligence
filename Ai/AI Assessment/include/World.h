#pragma once
#include <vector>
#include "Camera.h"
#include "Tree.h"
#include "Farm.h"
#include "Well.h"
#include "Warehouse.h"
#include "Farmer.h"
#include "WellCollector.h"
#include "Lumberjack.h"
#include "Builder.h"
#include "House.h"
#include "Terrain.h"

class World
{
public:
	World();
	~World();

	/*Initializes the world, spawning pawns, forests, wells, etc.*/
	void Init(const Camera& a_camera, Terrain& a_terrain);
	/*Runs every frame to update pawns.*/
	void Update(float a_deltaTime, Terrain& a_terrain);
	/*Runs every frame to draw scene elements.*/
	void Draw(Debugger& a_debugger, const Camera& a_camera);

	/*Creates several grouped trees at a_position. Takes in terrain to set the y.*/
	void CreateForest(Terrain& a_terrain, glm::vec2& a_position, int a_size, int a_chance = 25);
	/*Creates several unbuilt farms(invisible) at a_position. Takes in terrain to set the y.
	Farms need to be built before use*/
	void CreateFarmField(Terrain& a_terrain, glm::vec2& a_position, int a_size);
	/*Creates several unbuilt houses at a_position. Takes in terrain to set the y.*/
	void CreateEstate(Terrain& a_terrain, glm::vec2& a_position, int a_size, int chance = 50);
	/*Creates several unbuilt wells at a_position. Takes in terrain to set the y.
	Wells need to be built before use.*/
	void CreateWellField(Terrain& a_terrain, glm::vec2& a_position, int a_size, int chance = 50);
	/*Creates a warehouse at a_position. Takes in terrain to set the y.*/
	void SetWareHousePosition(Terrain& a_terrain, glm::vec2& a_position);

	/*Returns the nearest tree. If tree is occupied, does not return, continues iteration. If setOccupied is true, will set the tree to occupied*/
	Tree* GetNearestTree(glm::vec3& a_position, bool a_setOccupied = false);
	/*Returns the nearest Farm. If farm is occupied, does not return, continues iteration. If setOccupied is true, will set the farm to occupied*/
	Farm* GetNearestFarm(glm::vec3& a_position, bool a_setOccupied = false);
	/*Returns the nearest well.*/
	Well* GetNearestWell(glm::vec3& a_position);
	/*Returns the nearest house. If lookForBuilt is true, will look for a built house. If lookForFull is set to true, will look for a fullhouse*/
	House* GetNearestHouse(glm::vec3& a_position, bool a_lookForBuilt, bool a_lookForFull = false);
	/*Returns the warehouse.*/
	Warehouse* GetWarehouse() { return &m_Warehouse; }
private:
	/*Spawns a pawn or pawns taking in a number of pawns to spawn, if type is equal to "" or invalid entry, then a random pawn(s) will spawn at position, else spawns entered type of worker(s)*/
	void SpawnPawn(glm::vec3& a_position, int a_number, std::string a_type = "");

	Warehouse m_Warehouse;
	std::vector<Tree> m_Trees;
	std::vector<Farm> m_Farms;
	std::vector<Well> m_Wells;
	std::vector<House> m_Houses;

	std::vector<Farmer>m_Farmers;
	std::vector<WellCollector>m_WellCollectors;
	std::vector<Lumberjack>m_Lumberjacks;
	std::vector<Builder>m_Builders;
};