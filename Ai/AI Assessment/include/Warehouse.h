#pragma once
#include "Site.h"

class Warehouse : public Site
{
public:
	Warehouse();
	Warehouse(float a_food, float a_water, int a_wood);
	~Warehouse();

	void Update(float a_deltaTime);
	void Draw(Debugger& a_debugger);

	/*Returns how much food a pawn can take.*/
	float TakeFood(float a_food);
	/*Returns how much water a pawn can take.*/
	float TakeWater(float a_water);
	/*Returns how much wood a pawn can take.*/
	int TakeWood(int a_wood);

	/*Adds food to the food count.*/
	void AddFood(float a_food);
	/*Adds water to the water count.*/
	void AddWater(float a_water);
	/*Adds wood to the wood count.*/
	void AddWood(int a_wood);

	/*Returns the food count.*/
	float GetFood() { return m_Food; }
	/*Returns the water count.*/
	float GetWater() { return m_Water; }
	/*Returns the wood count.*/
	int GetWood() {return m_Wood; }

private:
	float m_Food;
	float m_Water;
	int m_Wood;
};