#pragma once
#include "Site.h"
class Farm : public Site
{
public:
	Farm(float a_maxFood);
	~Farm();

	void Update(float a_deltaTime);
	void Draw(Debugger& a_debugger);

	/*Adds food to the farm. Clamping to 0 and max.*/
	void AddFood(float a_food);
	/*Returns the number of food that a pawn can take from food count, depending on how much is left.*/
	float TakeFood(float a_food);

	/*Sets if the farm is growning.*/
	void SetIsGrowing(bool a_value);

	/*Returns the food count.*/
	float GetFood() { return m_Food; }
	/*Returns the max food count.*/
	float GetMaxFood() { return m_MaxFood; }
	/*Returns if famr is growing. Can not harvest if farm is growing.*/
	bool IsGrowing() { return m_IsGrowing; }

private:
	float m_MaxFood;
	float m_Food;

	bool m_IsGrowing;
};