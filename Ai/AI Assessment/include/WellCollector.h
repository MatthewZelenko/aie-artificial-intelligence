#pragma once
#include "Pawn.h"
class Well;
class WellCollector : public Pawn
{
public:
	WellCollector();
	~WellCollector();

	void Init(World* a_world, float a_maxCollectedWater, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed);
	void Update(float a_deltaTime);

	/*Sets the amount of water collected. Clamped to 0 and max.*/
	void SetCollectedWater(float a_value);
	/*Returns the max water a wellCollector can collect.*/
	void SetMaxCollectedWater(float a_max);

	/*Add Water to the count. Clamps to 0 and max.*/
	void AddCollectedWater(float a_value);

	/*Returns the Well the wellCollector is working at.*/
	Well* GetWell() { return m_Well; }

protected:
	void SelectAction();

	void DoAction(float a_deltaTime);

	float m_CollectedWater;
	float m_MaxCollectedWater;

	Well* m_Well;
};