#pragma once
#include "Site.h"
#include "Debugger.h"

class House : public Site
{
public:
	House(int a_maxResidents, int a_requiredLogs);
	~House();

	void Update(float a_deltaTime);
	void Draw(Debugger& a_debugger);

	/*Adds residents to the house. Returns the amount of residents it could add to the house. Sets to full when reaches max.*/
	int AddResidents(int a_number);
	/*Adds a log to the house, used in the building phase.*/
	void AddLog();

	/*Returns if the house is full.*/
	bool IsFull() { return m_IsFull; }
	/*Returns if the house is built.*/
	bool IsBuilt() { return m_IsBuilt; }

	/*Returns if the house should spawn people, set to true upon being built.*/
	bool ShouldSpawn() { return m_SpawnPeople; }
	/*Sets the spawn people bool to value.*/
	void SetShouldSpawn(bool m_value) { m_SpawnPeople = m_value; }

	/*Returns the number of logs the house has.*/
	int GetLogs() { return m_Logs; }
	/*Returns the number of required logs to build.*/
	int GetRequiredLogs() { return m_RequiredLogs; }

private:
	int m_MaxResidents;
	int m_Residents;
	bool m_IsFull;

	int m_RequiredLogs;
	int m_Logs;
	bool m_SpawnPeople;
	bool m_IsBuilt;
};