#pragma once
#include "Site.h"
class Well : public Site
{
public:
	Well(float a_maxWater);
	~Well();

	void Update(float a_deltaTime);
	void Draw(Debugger& a_debugger);

	/*Add water to the well. Clamps to 0 and max.*/
	void AddWater(float a_water);
	/*Returns how much water a pawn can take.*/
	float TakeWater(float a_water);

	/*Sets if the well is building.*/
	void SetIsBuilding(bool a_isBuilding) { m_IsBuilding = a_isBuilding; }

	/*Returns the water count.*/
	float GetWater() { return m_Water; }
	/*Returns the max water the well can have.*/
	float GetMaxWater() { return m_MaxWater; }
	/*Returns if the well is building.*/
	bool IsBuilding() { return m_IsBuilding; }

private:
	float m_MaxWater;
	float m_Water;


	bool m_IsBuilding;
};