#pragma once
#include "Pawn.h"
class House;
class Builder : public Pawn
{
public:
	Builder();
	~Builder();

	void Init(World* a_world, int a_maxCollectedWood, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed);
	void Update(float a_deltaTime);

	/*Sets how much wood the builder has. clamping to 0 and max.*/
	void SetCollectedWood(int a_value);
	/*Sets how much the wood the build can carry at one time.*/
	void SetMaxCollectedWood(int a_max);

	/*Adds wood to to the builders collection clamping to 0 and max.*/
	void AddCollectedWood(int a_value);

	/*Returns the builders construction site.*/
	House* GetConstructionSite() { return m_House; }

protected:
	void SelectAction();
	void DoAction(float a_deltaTime);

	int m_CollectedWood;
	int m_MaxCollectedWood;

	House* m_House;
};