#pragma once
#include "Site.h"
#include "Debugger.h"

class Tree : public Site
{
public:
	Tree(int a_wood);
	~Tree();

	void Update(float a_deltaTime);
	void Draw(Debugger& a_debugger);

	/*Returns how much wood a pawn can take.*/
	int TakeWood(int a_wood);
	/*Returns the wood count.*/
	int GetWood() { return m_Wood; }
	
private:
	int m_MaxWood;
	int m_Wood;
};