#pragma once
#include "Pawn.h"
class Tree;
class Lumberjack : public Pawn
{
public:
	Lumberjack();
	~Lumberjack();

	void Init(World* a_world, int a_maxCollectedWood, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed);
	void Update(float a_deltaTime);

	/*Sets the number of collected wood the lumberjack has. Clamps to 0 and max.*/
	void SetCollectedWood(int a_value);
	/*Sets the number of max wood the lumberjack can collect.*/
	void SetMaxCollectedWood(int a_max);

	/*Adds to the number of collected wood the lumberjack already has. Clamps to 0 and max.*/
	void AddCollectedWood(int a_value);

	/*Returns the tree the lumberjack is working at.*/
	Tree* GetTree() { return m_Tree; }

protected:
	void SelectAction();
	void DoAction(float a_deltaTime);

	int m_CollectedWood;
	int m_MaxCollectedWood;

	Tree* m_Tree;
};