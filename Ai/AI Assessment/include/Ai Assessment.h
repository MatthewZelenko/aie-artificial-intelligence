#pragma once
#include "Application.h"
#include "FPSCamera.h"
#include "Terrain.h"
#include "Debugger.h"
#include "World.h"

class AIAssessment :
	public Application
{
public:
	AIAssessment(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight);
	~AIAssessment();

	void Load();
	void ProcessInput();
	void Update();
	void Render();
	void UnLoad();

protected:
	Debugger m_Debugger;
	FPSCamera m_Camera;
	Terrain m_Terrain;
	Shader m_TerrainShader;

	World m_World;
};