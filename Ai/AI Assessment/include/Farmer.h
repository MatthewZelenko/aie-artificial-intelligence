#pragma once
#include "Pawn.h"
class Farm;
class Farmer : public Pawn
{
public:
	Farmer();
	~Farmer();

	void Init(World* a_world, float a_maxCollectedFood, float a_maxFood, float a_maxWater, float a_maxRest, float a_speed);
	void Update(float a_deltaTime);

	/*Sets the amount of collected food. Clamps to 0 and max.*/
	void SetCollectedFood(float a_value);
	/*Set the max amount the farmer can collect. Clamps to 0 and max.*/
	void SetMaxCollectedFood(float a_max);

	/*Adds to the collected amount of food. Clamps to 0 and max.*/
	void AddCollectedFood(float a_value);

	/*Returns the farmers farm.*/
	Farm* GetFarm() { return m_Farm; }

protected:
	void SelectAction();
	void DoAction(float a_deltaTime);

	float m_CollectedFood;
	float m_MaxCollectedFood;

	Farm* m_Farm;
};