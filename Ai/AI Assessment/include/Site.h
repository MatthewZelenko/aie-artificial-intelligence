#pragma once
#include <BoundingObject.h>
#include "Debugger.h"
class Site
{
public:
	Site(std::string a_name);
	virtual ~Site();
	
	virtual void Update(float a_deltaTime) = 0;
	virtual void Draw(Debugger& a_debugger) = 0;

	/*Setes the name of the site.*/
	void SetName(std::string& a_name);
	/*Sets the collision bounds of the site.*/
	void SetBound(BoundingObject* a_bound);
	/*Sets the position of the site.*/
	void SetPosition(glm::vec3& a_position);
	/*Sets the size of the site.*/
	void SetSize(glm::vec3& a_size);
	/*Sets the colour of the site.*/
	void SetColour(glm::vec4& a_colour);
	/*Sets whether the site is occupied.*/
	void SetIsOccupied(bool a_value);

	/*Increments the position to the site.*/
	void AddPosition(glm::vec3& a_position);
	
	/*Returns the name of the site.*/
	std::string	GetName() { return m_Name; }
	/*Returns the Collision bounds.*/
	BoundingObject* GetBoundObject() { return m_Bound; }
	/*Returns the position.*/
	glm::vec3 GetPosition() { return m_Position; }
	/*Returns the size.*/
	glm::vec3 GetSize() { return m_Size; }
	/*Returns the colour.*/
	glm::vec4 GetColour() { return m_Colour; }
	/*Returns whether or not the site is occupied.*/
	bool IsOccupied() { return m_IsOccupied; }

protected:
	std::string m_Name;
	BoundingObject* m_Bound;
	glm::vec3 m_Position;
	glm::vec3 m_Size;
	glm::vec4 m_Colour;

	bool m_IsOccupied;
};