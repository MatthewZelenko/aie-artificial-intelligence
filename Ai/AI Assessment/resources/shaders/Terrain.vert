#version 330 core

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 TexCoord;

uniform mat4 projection;
uniform mat4 view;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

void main()
{
	vTexCoord = TexCoord;
	vPosition = Position;
	vNormal = Normal;
	gl_Position = projection * view * vec4(vPosition, 1.0f);
}