#include "BoundingBox.h"

BoundingBox::BoundingBox()
{
	m_Type = BOX;
}
BoundingBox::~BoundingBox()
{
}

void BoundingBox::Fit(std::vector<glm::vec3>& a_points)
{
	for (auto& p: a_points)
	{
		//min
		if (p.x < m_Min.x) m_Min.x = p.x;
		if (p.y < m_Min.y) m_Min.y = p.y;
		if (p.z < m_Min.z) m_Min.z = p.z;
		//max
		if (p.x > m_Max.x) m_Max.x = p.x;
		if (p.y > m_Max.y) m_Max.y = p.y;
		if (p.z > m_Max.z) m_Max.z = p.z;
	}
}

void BoundingBox::SetMin(glm::vec3& a_value)
{
	m_Min = a_value;
}
void BoundingBox::SetMax(glm::vec3& a_value)
{
	m_Max = a_value;
}

void BoundingBox::SetPosition(glm::vec3 a_position)
{
	glm::vec3 size = m_Max - m_Min;
	m_Min = a_position;
	m_Max = m_Min + size;
}
void BoundingBox::AddPosition(glm::vec3 a_position)
{
	m_Min += a_position;
	m_Max += a_position;
}