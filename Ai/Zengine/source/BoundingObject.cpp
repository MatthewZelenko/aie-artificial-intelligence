#include "BoundingObject.h"

BoundingObject::BoundingObject()
{
	Reset();
}
BoundingObject::~BoundingObject()
{
}

void BoundingObject::Reset()
{
	m_Min.x = m_Min.y = m_Min.z = 1e37f;
	m_Max.x = m_Max.y = m_Max.z = -1e37f;
}