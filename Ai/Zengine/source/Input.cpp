#include "Input.h"
#include <string.h>
#include <assert.h>

Input* Input::m_Instance = nullptr;
Input::Input()
{
	memset(m_bKeys, false, sizeof(bool) * 1024);
	memset(m_bOldKeys, false, sizeof(bool) * 1024);
	memset(m_bOldMouseButton, false, sizeof(bool) * 8);
	memset(m_bMouseButton, false, sizeof(bool) * 8);
	m_dOldMousePosX = 0;
	m_dOldMousePosY = 0;
	m_dMousePosX = 0;
	m_dMousePosY = 0;
}
Input::~Input()
{
}
void Input::Create()
{
	if (!m_Instance)
		m_Instance = new Input();
}
void Input::Destroy()
{
	if (m_Instance)
		delete m_Instance;
}

void Input::KeyCallBack(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
	assert(m_Instance != nullptr);
	if (a_key > 0 && a_key < 1024)
	{
		if (a_action == GLFW_PRESS)
			m_Instance->m_bKeys[a_key] = true;
		else if (a_action == GLFW_RELEASE)
			m_Instance->m_bKeys[a_key] = false;
	}
}
void Input::CursorCallBack(GLFWwindow* a_window, double a_x, double a_y)
{
	assert(m_Instance != nullptr);
	m_Instance->m_dMousePosX = a_x;
	m_Instance->m_dMousePosY = a_y;
}
void Input::MouseCallBack(GLFWwindow* a_window, int a_button, int a_action, int a_mods)
{
	assert(m_Instance != nullptr);
	if (a_button >= 0 && a_button < 8)
	{
		if (a_action == GLFW_PRESS)
			m_Instance->m_bMouseButton[a_button] = true;
		else if (a_action == GLFW_RELEASE)
			m_Instance->m_bMouseButton[a_button] = false;
	}
}
void Input::UpdateInputs()
{
	assert(m_Instance != nullptr);
	memcpy(m_Instance->m_bOldKeys, m_Instance->m_bKeys, sizeof(bool) * 1024);
	memcpy(m_Instance->m_bOldMouseButton, m_Instance->m_bMouseButton, sizeof(bool) * 8);
	m_Instance->m_dOldMousePosX = m_Instance->m_dMousePosX;
	m_Instance->m_dOldMousePosY = m_Instance->m_dMousePosY;
}
bool Input::KeyDown(int a_key)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldKeys[a_key] == true && m_Instance->m_bKeys[a_key] == true)
	{
		return true;
	}
	return false;
}
bool Input::KeyUp(int a_key)
{
	assert(m_Instance != nullptr);
	if (!m_Instance->m_bOldKeys[a_key] && !m_Instance->m_bKeys[a_key])
	{
		return true;
	}
	return false;
}
bool Input::KeyPressed(int a_key)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldKeys[a_key] == false && m_Instance->m_bKeys[a_key] == true)
	{
		return true;
	}
	return false;
}
bool Input::KeyReleased(int a_key)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldKeys[a_key] && !m_Instance->m_bKeys[a_key])
	{
		return true;
	}
	return false;
}

bool Input::MouseDown(int a_button)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldMouseButton[a_button] == true && m_Instance->m_bMouseButton[a_button] == true)
	{
		return true;
	}
	return false;
}
bool Input::MouseUp(int a_button)
{
	assert(m_Instance != nullptr);
	if (!m_Instance->m_bOldMouseButton[a_button] && !m_Instance->m_bMouseButton[a_button])
	{
		return true;
	}
	return false;
}
bool Input::MousePressed(int a_button)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldMouseButton[a_button] == false && m_Instance->m_bMouseButton[a_button] == true)
	{
		return true;
	}
	return false;
}
bool Input::MouseReleased(int a_button)
{
	assert(m_Instance != nullptr);
	if (m_Instance->m_bOldMouseButton[a_button] && !m_Instance->m_bMouseButton[a_button])
	{
		return true;
	}
	return false;
}
void Input::SetMousePosition(GLFWwindow* a_window, float a_x, float a_y)
{
	assert(m_Instance != nullptr);
	glfwSetCursorPos(a_window, (double)a_x, (double)a_y);
	m_dMousePosX = a_x;
	m_dMousePosY = a_y;
	m_dOldMousePosX = m_dMousePosY;
	m_dOldMousePosY = m_dMousePosX;
}

double Input::GetMouseOffsetX()
{
	assert(m_Instance != nullptr);
	return m_Instance->m_dMousePosX - m_Instance->m_dOldMousePosX;
}
double Input::GetMouseOffsetY()
{
	assert(m_Instance != nullptr);
	return m_Instance->m_dOldMousePosY - m_Instance->m_dMousePosY;
}
double Input::GetMousePosX()
{
	assert(m_Instance != nullptr);
	return m_Instance->m_dMousePosX;
}
double Input::GetMousePosY()
{
	assert(m_Instance != nullptr);
	return m_Instance->m_dMousePosY;
}

Input* Input::Get()
{
	assert(m_Instance != nullptr);
	return m_Instance;
}