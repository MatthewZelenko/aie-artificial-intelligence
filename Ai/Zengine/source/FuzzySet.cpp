#include "FuzzySet.h"
#include "FuzzyMember.h"
#include <GLM\glm.hpp>

namespace Fuzzy
{
	Set::Set()
	{
	}
	Set::~Set()
	{

	}
	void Set::Destroy()
	{
		for (auto iter : m_Members)
		{
			delete iter.second;
		}
	}

	Member* Set::operator[](std::string a_name)
	{
		if (m_Members.find(a_name) != m_Members.end())
		{
			return m_Members[a_name];
		}
		return nullptr;
	}

	void Set::AddMember(Member* a_member)
	{
		if (m_Members.find(a_member->GetName()) != m_Members.end())
		{
			delete m_Members[a_member->GetName()];
		}
		m_Members[a_member->GetName()] = a_member;
	}

	void Set::CalculateDOM(float a_value)
	{
		for (auto member : m_Members)
		{
			member.second->SetDOM(member.second->CalculateDOM(a_value));
		}
	}
}