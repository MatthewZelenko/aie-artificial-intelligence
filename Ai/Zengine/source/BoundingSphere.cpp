#include "BoundingSphere.h"

BoundingSphere::BoundingSphere()
{
	m_Type = SPHERE;
}
BoundingSphere::~BoundingSphere()
{
}

void BoundingSphere::Fit(std::vector<glm::vec3>& a_points)
{
	for (auto& p : a_points)
	{
		//min
		if (p.x < m_Min.x) m_Min.x = p.x;
		if (p.y < m_Min.y) m_Min.y = p.y;
		if (p.z < m_Min.z) m_Min.z = p.z;
		//max
		if (p.x > m_Max.x) m_Max.x = p.x;
		if (p.y > m_Max.y) m_Max.y = p.y;
		if (p.z > m_Max.z) m_Max.z = p.z;
	}
	m_Center = (m_Min + m_Max) / 2.0f;
	m_Radius = glm::distance(m_Min, m_Center);
}

void BoundingSphere::SetCenter(glm::vec3 a_value)
{
	m_Center = a_value;
}
void BoundingSphere::SetRadius(float a_value)
{
	m_Radius = a_value;
}
void BoundingSphere::ScaleRadius(float a_scale)
{
	m_Radius *= a_scale;
}