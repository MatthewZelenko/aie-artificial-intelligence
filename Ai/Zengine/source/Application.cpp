#include "Application.h"
#include "Input.h"
#include <iostream>
#include <thread>

Application::Application(std::string a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight) : m_GameTitle(a_gameTitle), m_GameWidth(a_gameWidth), m_GameHeight(a_gameHeight), m_GameIsRunning(true), m_RunTime(0.0f), m_DeltaTime(0.0f), m_GameWindow(nullptr)
{
}
Application::~Application()
{
}

void Application::Run()
{
	SystemStartup();
	Load();

	glfwMakeContextCurrent(nullptr);
	std::thread renderThread([this]()
	{
		glfwMakeContextCurrent(m_GameWindow);
		while (m_GameIsRunning && !glfwWindowShouldClose(m_GameWindow))
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			Render();
			glfwSwapBuffers(m_GameWindow);
		}
	});
	while (m_GameIsRunning && !glfwWindowShouldClose(m_GameWindow))
	{
		SystemUpdate();
		ProcessInput();
		Update();

		if (Input::Get()->KeyDown(GLFW_KEY_ESCAPE))
		{
			m_GameIsRunning = false;
		}
	}
	renderThread.join();
	glfwMakeContextCurrent(m_GameWindow);
	UnLoad();
	SystemShutDown();
}

void Application::SystemStartup()
{
	bool success = false;
	int error = glfwInit();
	if (error == GL_FALSE)
	{
		std::cout << "Error initializing GLFW." << std::endl;
	}
	else
	{
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		m_GameWindow = glfwCreateWindow(m_GameWidth, m_GameHeight, m_GameTitle.c_str(), nullptr, nullptr);
		if (!m_GameWindow)
		{
			std::cout << "Error creating GLFWwindow." << std::endl;
		}
		else
		{
			glfwMakeContextCurrent(m_GameWindow);
			error = ogl_LoadFunctions();
			if (error == ogl_LoadStatus::ogl_LOAD_FAILED)
			{
				std::cout << "Error initializing gl_core." << std::endl;
			}
			else
			{
				glfwSwapInterval(1);
				glEnable(GL_CULL_FACE);
				glCullFace(GL_FRONT);
				
				glViewport(0, 0, m_GameWidth, m_GameHeight);
				glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				Input::Create();
				Input::Get()->SetMousePosition(m_GameWindow, m_GameWidth / 2.0f, m_GameHeight / 2.0f);
				glfwSetKeyCallback(m_GameWindow, Input::KeyCallBack);
				glfwSetCursorPosCallback(m_GameWindow, Input::CursorCallBack);
				glfwSetMouseButtonCallback(m_GameWindow, Input::MouseCallBack);

				success = true;
			}
		}
	}	
	if (!success)
	{
		system("Pause");
		m_GameIsRunning = false;
	}
}
void Application::SystemUpdate()
{
	double currentTime = glfwGetTime();
	m_DeltaTime = currentTime - m_RunTime;
	m_RunTime = currentTime;
	Input::Get()->UpdateInputs();
	glfwPollEvents();
}
void Application::SystemShutDown()
{
	Input::Destroy();
	glfwTerminate();
}