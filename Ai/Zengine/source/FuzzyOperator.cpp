#include "FuzzyOperator.h"
#include <assert.h>
#include <GLM\glm.hpp>

namespace Fuzzy
{
#pragma region And
	And::And(Term* a_t0, Term* a_t1) : Term('A')
	{
		m_DOM = 0.0f;
		m_Terms[0] = a_t0;
		m_Terms[1] = a_t1;
	}
	And::~And()
	{
		
	}
	void And::Destroy()
	{
		for (auto term : m_Terms)
		{
			if (term->GetType() == 'A' || term->GetType() == 'O')
			{
				delete term;
			}
		}
	}

	float And::GetDOM()
	{
		return glm::min(m_Terms[0]->GetDOM(), m_Terms[1]->GetDOM());
	}
	float And::ORWithDOM(float a_value)
	{
		m_Terms[0]->ORWithDOM(a_value);
		m_Terms[1]->ORWithDOM(a_value);
		return GetDOM();
	}
#pragma endregion And
#pragma region Or
	Or::Or(Term* a_t0, Term* a_t1) : Term('O')
	{
		m_DOM = 0.0f;
		m_Terms[0] = a_t0;
		m_Terms[1] = a_t1;
	}
	Or::~Or()
	{
		
	}
	void Or::Destroy()
	{
		for (auto term : m_Terms)
		{
			if (term->GetType() == 'A' || term->GetType() == 'O')
			{
				delete term;
			}
		}
	}
	float Or::GetDOM()
	{
		return glm::max(m_Terms[0]->GetDOM(), m_Terms[1]->GetDOM());
	}
	float Or::ORWithDOM(float a_value)
	{
		m_Terms[0]->ORWithDOM(a_value);
		m_Terms[1]->ORWithDOM(a_value);
		return GetDOM();
	}
#pragma endregion Or
}