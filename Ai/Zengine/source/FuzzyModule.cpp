#include "FuzzyModule.h"
#include "FuzzyMember.h"
#include <GLM\glm.hpp>

namespace Fuzzy
{
	Module::Module()
	{

	}
	Module::~Module()
	{
		
	}

	void Module::Destroy()
	{
		for (auto iter : m_Rules)
		{
			iter.Destroy();
		}
		m_Rules.clear();
		for (auto iter : m_Sets)
		{
			iter.second.Destroy();
		}
		m_Sets.clear();
	}

	Set& Module::operator[](std::string a_name)
	{
		return m_Sets[a_name];
	}

	void Module::ZeroConsequences()
	{
		for (auto iter : m_Rules)
		{
			iter.m_Consequence->SetDOM(0.0f);
		}
	}

	Set& Module::CreateSet(std::string a_name)
	{
		return m_Sets[a_name];
	}

	void Module::AddRule(Term* a_antecedent, Term* a_consequence)
	{
		m_Rules.push_back(Rule(a_antecedent, a_consequence));
	}

	float Module::Defuzzify(DefuzzifyType a_type, Set& a_output)
	{
		ZeroConsequences();
		//Iterate all rules calculating
		for (auto rule : m_Rules)
		{
			rule.CalculateDOM();
		}

		if (a_type == MAXAV)
		{
			Member* max = nullptr;
			for (auto iter : a_output.m_Members)
			{
				Member* current = iter.second;
				if (max == nullptr || current->GetDOM() > max->GetDOM())
				{
					max = iter.second;
				}
			}
			if (max)
			{
				return max->GetMaxDOM();
			}
		}
		if (a_type == CENTROID)
		{
			std::vector<float> values;
			float interval = 5.0f;
			float count = interval;
			float endValue = 0.0f;
			for (auto iter : a_output.m_Members)
			{
				if(iter.second->m_EndX > endValue)
					endValue = iter.second->m_EndX;
			}
			float numerator = 0.0f;
			float denominator = 0.0f;

			while (count <= endValue)
			{
				float result = 0.0f;
				for (auto iter : a_output.m_Members)
				{
					result += glm::min(iter.second->CalculateDOM(count), iter.second->m_DOM);
				}
				denominator += result;
				numerator += result * count;
				count += interval;
			}
			if (denominator != 0.0f)
				return numerator / denominator;
		}
		return 0.0f;
	}
}