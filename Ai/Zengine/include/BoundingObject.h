#pragma once
#include <GLM\glm.hpp>
#include <vector>

enum BoundType
{
	BOX,
	SPHERE
};
class BoundingObject
{
public:
	BoundingObject();
	virtual ~BoundingObject();

	/*Resets the minimum and maximum of the bounding object to its maximum opposites*/
	void Reset();
	/*Takes in positions and creates a bounding object surrounding all of them.*/
	virtual void Fit(std::vector<glm::vec3>& a_points) = 0;

	/*Returns the type of the bounding object.*/
	BoundType GetType() { return m_Type; }
	/*Returns the minimum of the bounding object.*/
	glm::vec3 GetMin() { return m_Min; }
	/*Returns the maximum of the bounding object.*/
	glm::vec3 GetMax() { return m_Max; }

protected:
	glm::vec3 m_Min, m_Max;
	BoundType m_Type;
};