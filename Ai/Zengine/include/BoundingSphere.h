#pragma once
#include "BoundingObject.h"

class BoundingSphere : public BoundingObject
{
public:
	BoundingSphere();
	~BoundingSphere();

	/*Fits the sphere around the points.*/
	void Fit(std::vector<glm::vec3>& a_points);

	/*Sets the center of the sphere.*/
	void SetCenter(glm::vec3 a_value);
	/*Sets the radius of the sphere.*/
	void SetRadius(float a_value);
	/*Multiplies the radius by set amount.*/
	void ScaleRadius(float a_scale);

	/*Returns the center of the sphere.*/
	glm::vec3 GetCenter() { return m_Center; }
	/*Returns the radius of the sphere.*/
	float GetRadius() { return m_Radius; }

private:
	glm::vec3 m_Center;
	float m_Radius;
};