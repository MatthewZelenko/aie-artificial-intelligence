#pragma once
#include "Camera.h"
#include <GLFW\glfw3.h>

class FPSCamera : public Camera
{
public:
	FPSCamera();
	FPSCamera(GLFWwindow* a_window, float a_acceleration, float a_maxSpeed, float a_mouseSensitivity, float a_mass);
	~FPSCamera();

	void Update(float a_deltaTime);
	/*Sets the movement keys.*/
	void SetInputKeys(int a_forwardKey, int a_backwardKey, int a_leftKey, int a_rightKey, int a_jumpKey, int a_controlEnableKey);
	/*Sets the look sensitivity.*/
	void SetMouseSensitivity(float a_mouseSensitivity);
	/*Sets the movement speed.*/
	void SetAcceleration(float a_acceleration);

	/*Sets if the camera is on the ground*/
	void SetIsGrounded(bool a_value);
	/*Sets the max velocity*/
	void SetMaxSpeed(float a_value);
	/*Sets the mass*/
	void SetMass(float a_value);
	/*Sets the jumping power*/
	void SetJumpPower(float a_value);

	glm::vec3 GetVelocity() { return m_Velocity; }

protected:
	int m_ForwardKey;
	int m_BackwardKey;
	int m_LeftKey;
	int m_RightKey;
	int m_JumpKey;
	int m_ControlEnableKey;
	bool m_ControlEnabled;
	float m_MouseSensitivity;

	float m_Mass;
	float m_Acceleration, m_MaxSpeed;
	glm::vec3 m_Velocity, m_TempVelocity;
	bool m_IsGrounded;
	float m_JumpPower;

	GLFWwindow* m_Window;
};