#pragma once
#include "BoundingObject.h"

class BoundingBox : public BoundingObject
{
public:
	BoundingBox();
	~BoundingBox();

	/*Fits the box around the positions.*/
	void Fit(std::vector<glm::vec3>& a_points);

	/*Sets the position of the box.*/
	void SetPosition(glm::vec3 a_position);
	/*Moves the box by set amount.*/
	void AddPosition(glm::vec3 a_position);

	/*Manually set the minimum of the box.*/
	void SetMin(glm::vec3& a_value);
	/*Manually set the maximum of the box.*/
	void SetMax(glm::vec3& a_value);
};