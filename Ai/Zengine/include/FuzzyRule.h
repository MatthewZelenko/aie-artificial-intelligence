#pragma once
#include "FuzzyTerm.h"

namespace Fuzzy
{
	class Module;
	class Rule
	{
	public:
		friend Module;
		Rule(Term* a_antecedents, Term* a_consequence);
		~Rule();
		void Destroy();

		float CalculateDOM();
	private:
		Term* m_Antecedents, *m_Consequence;
	};
}