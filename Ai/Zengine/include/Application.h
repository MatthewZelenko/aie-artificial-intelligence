#pragma once
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#include "Debugger.h"
#include <string>


class Application
{
public:
	Application(std::string	a_gameTitle, unsigned int a_gameWidth, unsigned int a_gameHeight);
	virtual ~Application();

	/*Call this function in main loop only.
	Will run ProcessInput(), Update(), Render() in a loop
	and run Load() and UnLoad() once.*/
	void Run();

protected:
	//Runs once upon startup.
	virtual void Load() = 0;
	//The input loop.
	virtual void ProcessInput() = 0;
	//Main game loop.
	virtual void Update() = 0;
	//The rendering loop.
	virtual void Render() = 0;
	//Runs once upon shutdown.
	virtual void UnLoad() = 0;

	//Returns the time change between frames.
	double GetDeltaTime() { return m_DeltaTime; }
	//Return current running game time.
	double GetRunTime() { return m_RunTime; }
	
	bool m_GameIsRunning;
	GLFWwindow* m_GameWindow;
	unsigned int m_GameWidth, m_GameHeight;

private:
	//For initalizing glfw, glew, etc.
	void SystemStartup();
	//For updating deltaTime, etc.
	void SystemUpdate();
	//CleanUp function on exit.
	void SystemShutDown();

	std::string	m_GameTitle;
	double m_DeltaTime;
	double m_RunTime;
};