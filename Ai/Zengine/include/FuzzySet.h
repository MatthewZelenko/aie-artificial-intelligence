#pragma once
#include <map>

namespace Fuzzy
{
	class Member;
	class Module;
	class Set
	{
	public:
		friend Module;
		Set();
		~Set();
		void Destroy();

		/*Returns a member with that name*/
		Member* operator[](std::string a_name);

		/*Calculates the DOMs of all the members that the rules use to access.*/
		void CalculateDOM(float a_value);

		/*Adds a member: Triangle, Shoulders...
		Must call Destroy to delete all members*/
		void AddMember(Member* a_member);

	private:
		std::map<std::string, Member*> m_Members;
	};
}