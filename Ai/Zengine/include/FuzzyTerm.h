#pragma once

namespace Fuzzy
{
	class Term
	{
	public:
		Term(char a_type);
		virtual ~Term();
		virtual void Destroy();

		void SetDOM(float a_DOM);
		virtual float ORWithDOM(float a_value) = 0;

		virtual float GetDOM() { return m_DOM; }
		char GetType() { return m_Type; }

	protected:
		float m_DOM;
		char m_Type;
	};
}