![Banner](https://bytebucket.org/MatthewZelenko/artificial-intelligence-fuzzy-logic/raw/38e2b77ce37d4f1a53302ba91e9b6f2a41c06f78/Ai%20Banner.jpg)

###OpenGL AI Project###
This was created using Visual Studio 2015.

Summary:
This is a small demo, and requires no interaction from the user. However the user can move around and observe.
This project demonstrates fuzzy logic. 

Requirements:
Download, open in Visual Studio, run.
Alternatively you can download the build [here](https://drive.google.com/uc?export=download&id=0BzmWDCip_PJLQnE2RV9laFJ2MVU)